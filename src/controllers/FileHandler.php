<?php

namespace Controllers;


class FileHandler
{
    public function saveToFile(string $filePath, string $contentType, $content): void
    {
        if ($contentType === 'html') {
            $format = '.html';
        }

        if ($contentType === 'json') {
            $format = '.json';
        }

        if ($contentType === 'text') {
            $format = '.text';
        }

        $fp = fopen($filePath . $format, 'wb');
        fwrite($fp, $content);
        fclose($fp);
    }

    public function makeDirectory(string $path): void
    {
        if (!mkdir($path, 0777, true) || !is_dir($path)) {
            die('Failed to create directory');
        }
    }
}