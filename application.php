<?php
date_default_timezone_set('Europe/Warsaw');

require 'src/controllers/RequestHandler.php';
require 'src/controllers/FileHandler.php';
require '_lib/simplehtmldom_1_8_1/simple_html_dom.php';

$requestHandler = new \Controllers\RequestHandler();
$fileHandler = new \Controllers\FileHandler();

const ITUNES_BASE_URL = 'https://itunes.apple.com/';
const GPLAY_BASE_URL = 'https://play.google.com/store/apps/developer';

$stores = [];
$responses = [];
$directory = date('Y-m-d_H-i-s');
$publishersPath = __DIR__ . '/input/publishers.json';
$localizationsPath = __DIR__ . '/input/localizations.json';
$publishers = json_decode(file_get_contents($publishersPath), true);
$localizations = json_decode(file_get_contents($localizationsPath), true);

foreach ($publishers as $publisher) {
    foreach ($localizations as $localization) {
        $storeURL = ITUNES_BASE_URL . $localization['cc'] . '/developer' . $publisher['itunes_path_to_publisher'];
        $responses[$localization['cc']]['html'] = $requestHandler->sendRequest($storeURL);
        $responses[$localization['cc']]['cc'] = $localization['cc'];
        $stores[$localization['cc']]['url'] = $storeURL;
        $stores[$localization['cc']]['cc'] = $localization['cc'];
    }

    foreach ($responses as $response) {
        $htmlDOM = str_get_html($response['html']);
        $index = 0;

        if (!empty($htmlDOM)) {
            foreach($htmlDOM->find('div[id^="we-lockup-labelled-by-"] div') as $element) {
                $title = $element->plaintext;
                $stores[$response['cc']]['apps'][$title]['index'] = $index++;
                $stores[$response['cc']]['apps'][$title]['title'] = $title;
            }

            foreach ($stores[$response['cc']]['apps'] as $element) {
                $link = $htmlDOM->find('a[href^="https://itunes.apple.com/"]', $element['index'])->href;
                $stores[$response['cc']]['apps'][$element['title']]['link'] = $link;
            }
        }
    }

    $output = json_encode($stores, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
    $fileHandler->makeDirectory('output/' . $directory);
    $fileHandler->saveToFile('output/' . $directory . '/' . strtolower($publisher['name']), 'json', $output);
}